import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import SignupForm from "./SignupForm.js";
import LoginForm from "./LoginForm";
import ExerciseForm from "./ExerciseForm";
import ExerciseList from "./ExerciseList";

const domain = /https:\/\/[^/]+/;
const basename = process.env.PUBLIC_URL.replace(domain, "");
const baseUrl = process.env.REACT_APP_USER_SERVICE_API_HOST;

function App() {
  return (
    <BrowserRouter basename={basename}>
      <AuthProvider baseUrl={baseUrl}>
        <Routes>
          <Route path="signup" element={<SignupForm />} />
          <Route path="login" element={<LoginForm />} />
          <Route path="exercise-form" element={<ExerciseForm />} />
          <Route path="exercise-list" element={<ExerciseList />} />
        </Routes>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
