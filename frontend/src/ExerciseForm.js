import React, { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";

export default function ExerciseForm() {
  const { token } = useToken();

  const [exercise, setExercise] = useState("");
  const [muscle, setMuscle] = useState("");
  const [date, setDate] = useState("");

  const handleExerciseChange = (e) => {
    const value = e.target.value;
    setExercise(value);
  };

  const handleMuscleChange = (e) => {
    const value = e.target.value;
    setMuscle(value);
  };

  const handleDateChange = (e) => {
    const value = e.target.value;
    setDate(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {};

    data.exercise = exercise;
    data.muscle = muscle;
    data.date = date;

    const exerciselUrl = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/exercises`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };
    const response = await fetch(exerciselUrl, fetchConfig);

    if (response.ok) {
      setExercise("");
    } else {
      console.error("Could not create exercise");
    }
  };
  return (
    <div className="rounded-xl z-100">
      <div className="w-full justify-center px-2">
        <h1 className="text-center headers text-2xl px-4 mb-2">New Exercise</h1>
        <form onSubmit={handleSubmit} id="create-exercise-form">
          <textarea
            className="shadow appearance-none border rounded w-full p-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline resize-none"
            onChange={handleMuscleChange}
            value={muscle}
            placeholder="Muscle"
          />
          <textarea
            className="shadow appearance-none border rounded w-full p-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline resize-none"
            onChange={handleExerciseChange}
            value={exercise}
            placeholder="Exercise"
          />
          <textarea
            className="shadow appearance-none border rounded w-full p-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline resize-none"
            onChange={handleDateChange}
            value={date}
            placeholder="(MM/DD/YYYY)"
          />
          <div className="text-center">
            <button className="m-2 shadow focus:shadow-outline focus:outline-none text-black py-2 px-4 rounded-2xl">
              Create
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
