import { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";

export default function ExerciseList() {
  const { token } = useToken();
  const [exercises, setExercises] = useState([]);

  useEffect(() => {
    const fetchExercises = async () => {
      if (token) {
        const exercisesUrl = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/exercises`;

        const fetchConfig = {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        };
        const response = await fetch(exercisesUrl, fetchConfig);

        if (response.ok) {
          const data = await response.json();
          setExercises(data);
        }
      }
    };
    fetchExercises();
  }, [token]);

  return (
    <>
      <div class="max-w-sm rounded overflow-hidden shadow-lg">
        <div class="px-6 py-4">
          <div class="font-bold text-xl mb-2">
            {exercises.map((exercise) => {
              return (
                <tr key={exercise.id}>
                  <td>{exercise.muscle}</td>
                  <td>{exercise.exercise}</td>
                </tr>
              );
            })}
          </div>
          <p class="text-gray-700 text-base">Testing</p>
        </div>
      </div>
    </>
  );
}
