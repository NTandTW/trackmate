from fastapi import FastAPI
from routers import exercises, accounts, range
from authenticator import authenticator
from fastapi.middleware.cors import CORSMiddleware
import os

app = FastAPI()

origins = [
    os.environ.get("CORS_HOST", None), "http://localhost:3000"
]

app.include_router(accounts.router)
app.include_router(authenticator.router)
app.include_router(exercises.router)
app.include_router(range.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
