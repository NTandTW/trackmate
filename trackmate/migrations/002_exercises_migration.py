steps = [
    [
        """
        CREATE TABLE exercises(
            id SERIAL PRIMARY KEY NOT NULL,
            account_id INT NOT NULL,
            exercise TEXT,
            muscle TEXT,
            date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
            );
        """,
        """
        DROP TABLE exercises
        """,
    ]
]
