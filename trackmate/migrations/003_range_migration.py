steps = [
    [
        """
        CREATE TABLE ranges(
            id SERIAL PRIMARY KEY NOT NULL,
            account_id INT NOT NULL,
            exercises_id INT NOT NULL,
            sets INT,
            reps INT,
            weight INT
            );
        """,
        """
        DROP TABLE ranges
        """,
    ]
]
