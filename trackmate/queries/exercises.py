from pydantic import BaseModel
from queries.pool import pool
import datetime
from typing import Union, Optional, List

class ExerciseIn(BaseModel):
    account_id: int
    exercise: Optional[str]
    muscle: Optional[str]
    date: Optional[datetime.date]

class ExerciseOut(ExerciseIn):
    id: int


class ExerciseQueries(BaseModel):
    def create(self, exercise: ExerciseIn, account_id:int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result =db.execute(
                        """
                        INSERT INTO exercises(
                            account_id,
                            exercise,
                            muscle,
                            date
                        )
                        VALUES ( %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            account_id,
                            exercise.exercise,
                            exercise.muscle,
                            exercise.date
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = exercise.dict()
                    return ExerciseOut(id=id, **old_data)
        except Exception:
            return {"message": "Couldnt create the exercise"}

    def get_all(self, exercise:ExerciseIn, account_id:int) -> List[ExerciseOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM exercises
                        WHERE exercises.account_id = %s
                        """,
                        [account_id],
                    )
                    exercises = []
                    for record in db:
                        exercise = ExerciseOut(
                            id=record[0],
                            account_id=record[1],
                            exercise=record[2],
                            muscle=record[3],
                            date=record[4],
                        )
                        exercises.append(exercise)
                    return exercises
        except Exception as e:
            raise e

    def get_one(self, account_id: int, exercise_id: int) -> ExerciseOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            account_id,
                            exercise,
                            muscle,
                            date
                        FROM exercises
                        WHERE id = %s
                        AND account_id = %s
                        """,
                        [exercise_id, account_id],
                    )
                    record = db.fetchone()
                    return ExerciseOut(
                        id=record[0],
                        account_id=record[1],
                        exercise=record[2],
                        muscle=record[3],
                        date=record[4],
                    )
        except Exception:
            return {"message": "Could not get exercise"}

    def update(self, account_id: int, exercise_id: int, exercise: ExerciseOut) -> ExerciseOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE exercises
                            set exercise = %s, muscle = %s
                        WHERE id = %s AND account_id = %s
                        """,
                        [
                            exercise.exercise,
                            exercise.muscle,
                            exercise_id,
                            account_id
                        ]
                    )
                    old_data = exercise.dict()
                    old_data["id"] = exercise_id
                    old_data["account_id"] = account_id
                    return ExerciseOut(**old_data)
        except Exception as e:
            raise e

    def delete(self, exercise_id: int, account_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE from exercises
                        WHERE id = %s
                        AND account_id = %s
                        """,
                        [exercise_id, account_id]
                    )
                    return True
        except Exception:
            return False
