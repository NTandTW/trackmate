from pydantic import BaseModel, ValidationError
from queries.pool import pool
from typing import Optional, Union, List

class RangeIn(BaseModel):
    account_id: int
    exercises_id: int
    sets: Optional[int]
    reps: Optional[int]
    weight: Optional[int]

class RangeOut(BaseModel):
    id: int
    account_id: int
    exercises_id: int
    sets: Optional[int]
    reps: Optional[int]
    weight: Optional[int]

class Error(BaseModel):
    message: str

class RangeQueries(BaseModel):
    def create(self, account_id: int, exercises_id: int, ranges: RangeIn) -> RangeOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute (
                        """
                        INSERT INTO ranges (
                            account_id,
                            exercises_id,
                            sets,
                            reps,
                            weight
                        )
                        VALUES ( %s, %s, %s, %s, %s)
                        RETURNING id ;
                        """,
                        [
                            account_id,
                            exercises_id,
                            ranges.sets,
                            ranges.reps,
                            ranges.weight,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = ranges.dict()
                    return RangeOut(id=id, **old_data)
        except Exception:
            return {"message": "Could not get the ranges for the exercise"}

    def get_all(self, account_id: int, exercises_id: int, range:RangeOut) -> List[RangeOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT r.id, r.account_id, r.exercises_id, r.sets, r.reps, r.weight
                        FROM ranges r
                        JOIN exercises e ON r.exercises_id = e.id
                        WHERE r.exercises_id = %s
                        AND r.account_id = %s
                        """,
                        [
                            account_id,
                            exercises_id,
                        ],
                    )
                    ranges = []
                    for record in db:
                        range = RangeOut(
                            id = record[0],
                            account_id = record[1],
                            exercises_id = record[2],
                            sets = record[3],
                            reps = record[4],
                            weight = record[5]
                        )
                        ranges.append(range)
                    return ranges
        except Exception as e:
            return [{"message": "Could not get the ranges for the exercise"}]

    def get_one(self, id: int, account_id: int) -> RangeOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        ranges.id,
                        ranges.account_id,
                        ranges.exercises_id,
                        ranges.sets,
                        ranges.reps,
                        ranges.weight
                        FROM ranges
                        WHERE id = %s AND account_id = %s
                        """,
                        [
                            id,
                            account_id
                        ],
                    )
                    record = db.fetchone()
                    if record is None:
                        return None
                    return RangeOut(
                        id = record[0],
                        account_id = record[1],
                        exercises_id = record[2],
                        sets = record[3],
                        reps = record[4],
                        weight = record[5]
                    )
        except Exception:
            return {"message": "Could not retreive info"}

    def delete(self, id: int, account_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE from ranges
                        WHERE id = %s AND account_id = %s
                        """,
                        [id, account_id],
                    )
                    return True
        except Exception:
            return False

    def update(self,range_id: int, account_id: int, range: RangeOut) -> RangeOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE ranges
                        SET sets = %s, reps = %s, weight = %s
                        WHERE id = %s AND account_id = %s
                        """,
                        [
                            range.sets,
                            range.reps,
                            range.weight,
                            range_id,
                            account_id,
                        ]
                    )
                    old_data = range.dict()
                    old_data["id"] = range_id
                    old_data["account_id"] = account_id
                    return RangeOut(**old_data)
        except ValidationError as e:
            print(e)
