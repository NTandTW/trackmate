from fastapi import APIRouter, Depends, Response
from queries.exercises import (
    ExerciseIn,
    ExerciseOut,
    ExerciseQueries,
)
from typing import List, Optional
from authenticator import authenticator

router = APIRouter()

@router.post("/api/exercises", response_model=ExerciseOut)
def create_exercises(
    info: ExerciseIn,
    repo: ExerciseQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.create(info, account_id)

@router.get("/api/exercises", response_model=List[ExerciseOut])
def all_exercises(
    repo: ExerciseQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data)
):
    account_id = account_data["id"]
    return repo.get_all(repo, account_id)


@router.get("/api/exercises/{id}", response_model=ExerciseOut)
def get_one_exercise(
    id: int,
    repo: ExerciseQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.get_one(account_id, id)

@router.put("/api/exercises/{id}", response_model=ExerciseOut)
def update(
    id: int,
    exercise: ExerciseIn,
    repo: ExerciseQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data)
):
    account_id = account_data["id"]
    return repo.update(account_id, id, exercise)

@router.delete("/api/exercises/{id}", response_model=bool)
def delete(
    id: int,
    repo: ExerciseQueries = Depends(),
    account_data=Depends(authenticator.get_current_account_data),
):
    account_id=account_data["id"]
    return repo.delete(id, account_id)
