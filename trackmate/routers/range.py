from fastapi import APIRouter, Depends, Response
from queries.range import (
    RangeIn,
    RangeOut,
    RangeQueries
)
from typing import List, Optional
from authenticator import authenticator

router = APIRouter()

@router.post("/api/exercises/{exercises_id}/range", response_model=RangeOut)
def create(
    exercises_id: int,
    info: RangeIn,
    repo: RangeQueries = Depends(),
    account_data= Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.create(account_id, exercises_id, info)

@router.get("/api/exercises/{exercises_id}/range", response_model=List[RangeOut])
def get_all(
    exercises_id: int,
    repo: RangeQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data)
):
    account_id = account_data["id"]
    return repo.get_all(account_id, exercises_id, repo)

@router.get("/api/ranges/{id}", response_model=RangeOut)
def get_one(
    id:int,
    repo: RangeQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data)
):
    account_id = account_data["id"]
    return repo.get_one(id, account_id)

@router.delete("/api/ranges/{id}", response_model=bool)
def delete(
    id: int,
    repo: RangeQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data),
):
    account_id=account_data["id"]
    return repo.delete(id, account_id)

@router.put("/api/ranges/{id}", response_model=RangeOut)
def update(
    id: int,
    info: RangeIn,
    repo: RangeQueries = Depends(),
    account_data = Depends(authenticator.get_current_account_data)
):
    account_id = account_data["id"]
    return repo.update(id, account_id, info)
